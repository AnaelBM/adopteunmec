<?php
/**
 * Created by PhpStorm.
 * User: Seamless
 * Date: 09/10/2019
 * Time: 10:42
 */



namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin_homepage")
     */
    public function adminHomepageAction()
    {
        return $this->render('@App/Admin/dashboard.html.twig', [
            'badges' => $this->getAllBadges(),
            'comments' => $this->getComments(),
            'users' => $this->getUsers()
        ]);
    }

    private function getComments(){
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository('AppBundle:Comment')->findBy(['validated'=>null]);
        return $comments;
    }

    private function getUsers(){
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findAll();
        return $users;
    }

    private function getAllBadges()
    {
        $em = $this->getDoctrine()->getManager();
        $badges = $em->getRepository('AppBundle:Badge')->findAll();
        $badges_array = [];
        foreach ($badges as $badge){
            array_push($badges_array, [
                'id' => $badge->getId(),
                'name' => $badge->getName(),
                'img' => '/assets/badges/'.$badge->getImg()
            ]);
        }
        return $badges_array;
    }

}