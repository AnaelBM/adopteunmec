<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 */
class Badge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProfilBadge", mappedBy="badgeId", orphanRemoval=true)
     */
    private $profilBadges;


    
    public function __construct()
    {
        $this->profilBadges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|ProfilBadge[]
     */
    public function getProfilBadges(): Collection
    {
        return $this->profilBadges;
    }

    public function addProfilBadge(ProfilBadge $profilBadge): self
    {
        if (!$this->profilBadges->contains($profilBadge)) {
            $this->profilBadges[] = $profilBadge;
            $profilBadge->setBadgeId($this);
        }

        return $this;
    }

    public function removeProfilBadge(ProfilBadge $profilBadge): self
    {
        if ($this->profilBadges->contains($profilBadge)) {
            $this->profilBadges->removeElement($profilBadge);
            // set the owning side to null (unless already changed)
            if ($profilBadge->getBadgeId() === $this) {
                $profilBadge->setBadgeId(null);
            }
        }

        return $this;
    }
}
